#!/usr/bin/env python3

'''
Daemonize BrowserStack Local
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

from browserstack.local import Local


def start_browserstack_local():
    '''
    Daemonize BrowserStack Local
    '''
    print('INFO: Starting BrowerStack local.')
    # Create an instance of BrowerStack local
    bs_local = Local()

    # Key is your BrowserStack password
    bs_local_args = {'key': '9A7XsA7QzPmspiSwrsUc',
                     'v': 'true',
                     'force': 'true',
                     'forcelocal': 'true',
                     'localIdentifier': 'daemonized_bs_local'}

    # Start local instance with the given parameters
    bs_local.start(**bs_local_args)

    # Print BrowserStack Local status
    print('BrowserStack Local is Running:', bs_local.isRunning())

if __name__ == "__main__":
    start_browserstack_local()
