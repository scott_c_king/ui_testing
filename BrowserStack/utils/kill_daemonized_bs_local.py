#!/usr/bin/env python3

'''
Kill Daemonized BrowserStack Local
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

from subprocess import check_call
from subprocess import check_output


def kill_browserstack_local():
    '''
    Kill Daemonized BrowserStack Local
    '''
    cmd = "ps -ef | grep -i browserstack | grep 'force ' | awk '{ print $2 }'"
    processes = check_output(cmd, shell=True).decode('utf-8')
    for process in processes.split('\n')[:-2]:
        print(check_call(f'kill {process}', shell=True))

if __name__ == "__main__":
    kill_browserstack_local()
