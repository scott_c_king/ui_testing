#!/usr/bin/env python3

'''
Behave Environment
'''

import json
import os
import sys
from selenium import webdriver


try:
    BROWSERS = os.environ['CONFIG_FILE']
    TASK_ID = int(os.environ['TASK_ID'])
except NameError:
    print('This script should not be run stand-alone, but by '
          'behave_runner.py.  Browser configs (.json) and task ID '
          'should be passed as environment variables.\n\n'
          'Example: CONFIG_FILE=browsers.json TASK_ID=0 behave '
          'features/single.feature')
    sys.exit(1)

with open(BROWSERS, 'r') as data_file:
    CONFIG = json.load(data_file)

BROWSERSTACK_USERNAME = CONFIG['user']
BROWSERSTACK_ACCESS_KEY = CONFIG['key']


def before_feature(context, feature):
    ''' Instantiate webdriver before each feature. '''
    capabilities = CONFIG['browsers'][TASK_ID]
    print('INFO: Desired Capabilities: ', capabilities)

    for key in CONFIG["capabilities"]:
        if key not in capabilities:
            capabilities[key] = CONFIG["capabilities"][key]

    bs_hub = ('https://scottcking1:9A7XsA7QzPmspiSwrsUc@'
              'hub-cloud.browserstack.com/wd/hub')
    context.browser = webdriver.Remote(desired_capabilities=capabilities,
                                       command_executor=bs_hub)


def after_feature(context, feature):
    ''' Quit webdriver instance after the feature. '''
    context.browser.quit()
