Feature: BrowserStack Behave - Local Testing
  
    Scenario: Verify Local platform-dev02 URL Is Accessible
        When visit url "https://platform.dragos.services/#/login"
        Then page contains "Copyright 2017-2020"
