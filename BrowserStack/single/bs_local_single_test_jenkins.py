#!/usr/bin/env python3

'''
BrowserStack Local Testing POC
'''

import os
import sys
import urllib3
#from browserstack.local import Local
from selenium import webdriver
urllib3.disable_warnings()

try:
    USERNAME = os.environ["BROWSERSTACK_USERNAME"]
    BS_ACCESS_KEY = os.environ["BROWSERSTACK_ACCESS_KEY"]
    BS_LOCAL = os.environ["BROWSERSTACK_LOCAL"]
    BS_LOCAL_ID = os.environ["BROWSERSTACK_LOCAL_IDENTIFIER"]
except KeyError:
    print('ERROR: Missing required Jenkins environment variables!')
    sys.exit(1)


def main():
    ''' BrowserStack Local Testing POC '''

    # BrowserStack Local Started by Jenkins!
    #bs_local = Local()
    #bs_local.start(**BS_LOCAL_ARGS)
    #print('BrowserStack Local is Running:', bs_local.isRunning())

    # Browser-specific settings
    capabilities = {'browser': 'Firefox',
                    'browser_version': '73.0',
                    'os': 'OS X',
                    'os_version': 'Catalina',
                    'resolution': '1920x1080',
                    'build': 'BS Local - Single Test in Dragos Jenkins',
                    'browserstack.localIdentifier': BS_LOCAL_ID,
                    'browserstack.local': BS_LOCAL}

    url = 'https://platform.dragos.services/#/login'
    bs_hub = ('https://' + USERNAME + ':' + BS_ACCESS_KEY +
              '@hub.browserstack.com/wd/hub')
    driver = webdriver.Remote(command_executor=bs_hub,
                              desired_capabilities=capabilities)

    driver.get(f'{url}')
    print('TEST CASE: Is Dragos found in page title?')
    try:
        assert 'Dragos' in driver.title
        print(f'PASS: Dragos is found in "{driver.title}" for URL {url}?')
    except AssertionError:
        print(f'FAIL: Dragos not found in "{driver.title}" for URL {url}.')
    finally:
        driver.quit()
        print('INFO: driver.quit() completed...')


if __name__ == "__main__":
    main()
