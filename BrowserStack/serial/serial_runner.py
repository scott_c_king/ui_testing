#!/usr/bin/env python3

'''
BrowserStack Test: Run (4) Instances Serially
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

import subprocess

CMDS = ['python3 bs_local_1.py | tee logs/bs_local_1.log',
        'python3 bs_local_2.py | tee logs/bs_local_2.log',
        'python3 bs_local_3.py | tee logs/bs_local_3.log',
        'python3 bs_local_4.py | tee logs/bs_local_4.log']


def main():
    ''' BrowserStack Test: Run (4) Instances Serially '''
    for cmd in CMDS:
        subprocess.check_output(cmd, shell=True)


if __name__ == "__main__":
    main()
