#!/usr/bin/env python3

'''
BrowserStack Local Testing POC
'''

import sys
import urllib3
from browserstack.local import Local
from selenium import webdriver
urllib3.disable_warnings()


# Key is your BrowserStack password
BS_LOCAL_ARGS = {'key': '9A7XsA7QzPmspiSwrsUc',
                 'v': 'true',
                 'force': 'true',
                 'forcelocal': 'true',
                 'localIdentifier': 'BS_Local_Testing'}


def main():
    ''' BrowserStack Local Testing POC '''

    # Start instance of BrowserStack Local
    bs_local = Local()
    bs_local.start(**BS_LOCAL_ARGS)
    print('BrowserStack Local is Running:', bs_local.isRunning())

    # Browser-specific settings
    capabilities = {'browser': 'Edge',
                    'browser_version': '80.0',
                    'os': 'Windows',
                    'os_version': '10',
                    'resolution': '1920x1080',
                    'build': 'BS Local - Serial Test',
                    'browserstack.localIdentifier': 'BS_Local_Testing',
                    'browserstack.local': 'true'}

    url = 'https://platform.dragos.services/#/login'
    bs_hub = ('https://scottcking1:9A7XsA7QzPmspiSwrsUc@'
              'hub-cloud.browserstack.com/wd/hub')
    driver = webdriver.Remote(command_executor=bs_hub,
                              desired_capabilities=capabilities)

    driver.get(f'{url}')
    print('TEST CASE: Is Dragos found in page title?')
    try:
        assert 'Dragos' in driver.title
        print(f'PASS: Dragos is found in "{driver.title}" for URL {url}?')
    except AssertionError:
        print(f'FAIL: Dragos not found in "{driver.title}" for URL {url}.')
        sys.exit(1)
    finally:
        print('INFO: Stopping webdriver...')
        driver.quit()
        print('INFO: Stopping BrowerStack Local...')
        bs_local.stop()
        print('INFO: BrowserStack Local is Running:', bs_local.isRunning())


if __name__ == "__main__":
    main()
