#!/usr/bin/env python3

'''
BrowserStack Local (4) Instance Parallel Test POC
'''

import json
import sys
import urllib3
from selenium import webdriver
urllib3.disable_warnings()


def import_json(browser_configs, process_index):
    '''
    Import JSON, return Python dictionary by process_index.
    '''
    with open(browser_configs, 'r') as jfile:
        data = json.loads(jfile.read())
    return data[process_index]


def main():
    ''' BrowserStack Local (4) Instance Parallel Test POC '''
    # Read browers.json, load correct capabilities index.
    capabilities = import_json(BROWSERS, PROCESS_INDEX)

    # Start Webdriver
    url = 'https://platform.dragos.services/#/login'
    bs_hub = ('https://scottcking1:9A7XsA7QzPmspiSwrsUc@'
              'hub-cloud.browserstack.com/wd/hub')
    driver = webdriver.Remote(command_executor=bs_hub,
                              desired_capabilities=capabilities)

    # Test
    driver.get(f'{url}')
    print(f'\nProcess [{PROCESS_INDEX}]: Is Dragos found in page title?')
    result = 'Process [{}]: {} - Dragos {} found in "{}" for URL {}.'
    try:
        assert 'Dragos' in driver.title
        print(result.format(PROCESS_INDEX, 'PASS', 'is', driver.title, url))
    except AssertionError:
        print(result.format(PROCESS_INDEX, 'FAIL', 'not', driver.title, url))
        sys.exit(1)
    finally:
        print(f'Process [{PROCESS_INDEX}]: Stopping webdriver...')
        driver.quit()


if __name__ == "__main__":
    try:
        BROWSERS = sys.argv[1]
        PROCESS_INDEX = int(sys.argv[2])
    except IndexError:
        print('This script should not be run stand alone, but by bs_prunner.'
              'Browser configs (.json) and process index should be passed'
              'as the first and second arguments, respectively.\n\n'
              'Example: python3 bs_ptest.py browsers.json process_index')
        sys.exit(1)
    main()
