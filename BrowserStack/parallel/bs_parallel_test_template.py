#!/usr/bin/env python3

'''
Dragos BrowserStack Selenium Runner
'''

__author__ = 'Scott C. King'
__version__ = '0.1.0'
__license__ = 'MIT'

import atexit
import os
import sys
import json
from browserstack.local import Local
from selenium import webdriver
from selenium.common.exceptions import WebDriverException


def housekeeping():
    ''' Clean up and/or kill instances of BrowserStack local
        and Selenium webdriver.
    '''
    DRIVER.quit()
    BS_LOCAL.stop()


def browserstack_local():
    ''' Browerstack local instance control.

    Args:
    action = start or stop BrowserStack local

    '''
    bs_local = Local()

    # Key is your BrowserStack password
    bs_local_args = {'key': '9A7XsA7QzPmspiSwrsUc',
                     'v': 'true',
                     'force': 'true',
                     'forcelocal': 'true',
                     'localIdentifier': 'SCKing_Testing'}

    # Start local instance with the given parameters
    bs_local.start(**bs_local_args)

    # Print BrowserStack local status
    return ('BrowserStack Local is Running:', bs_local.isRunning())


def main():
    ''' Do the work. '''
    atexit.register(housekeeping)
    print(browserstack_local('start'))
    sys.exit(0)


def import_json(json_file, counter=4):
    '''
    Import JSON, return Python dictionary.
    '''

    with open(json_file, 'r') as jfile:
        data = json.loads(jfile.read())
    print(data)
    instance_capabilities = data[int(counter)]
    print(instance_capabilities)
    print('Test %s started' % (counter_val))

#------------------------------------------------------#
# Mention any other capabilities required in the test
caps = {}
caps['browserstack.debug'] = 'true'
caps['build'] = 'parallel tests'

#------------------------------------------------------#

caps = {**caps, **instance_caps}

#------------------------------------------------------#
# THE TEST TO BE RUN PARALLELY GOES HERE

driver = webdriver.Remote(
    command_executor='https://%s:%s@hub.browserstack.com/wd/hub' % (
        USERNAME, BROWSERSTACK_ACCESS_KEY
    ),
    desired_capabilities=caps
)

driver.get('http://www.google.com')
inputElement = driver.find_element_by_name('q')
inputElement.send_keys('browserstack')
inputElement.submit()
print(driver.title)
driver.quit()
#------------------------------------------------------#


if __name__ == '__main__':
    ''' This is executed when run from the command line '''
    try:
        json_file = sys.argv[1]
        counter_val = sys.argv[2]
    except IndexError:
        print('Test name/suite and browsers.json must be passed as arguments.')
        sys.exit(1)
    USERNAME = os.environ.get('BROWSERSTACK_USERNAME') or sys.argv[2]
    BROWSERSTACK_ACCESS_KEY = os.environ.get('BROWSERSTACK_ACCESS_KEY') or sys.argv[3]
    main()
