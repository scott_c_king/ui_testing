#!/usr/bin/env python3

'''
Dragos Login Page Module
'''

import behave
import time
import os

import selenium as se
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


VALID_USERNAME = 'admin'
VALID_PASSWORD = 'Dr@gosSyst3m'

LOGIN_URL = "https://platform-dev10.dragos.services/#/login"

#field locator classes/ids/selectors
PASSWORD_FIELD_ID = 'password'
USERNAME_FIELD_ID = 'username'
LOGIN_BUTTON_SELECTOR = '#external-form > button'
ERROR_MESSAGE_CLASS = 'form-error'
SHOW_PASSWORD_BUTTON = '//*[@id="external-form"]/div[4]/a/small'
COPYRIGHT_CHECKBOX_XPATH = '//*[@id="external-form"]/div[5]/div[1]'
LICENSE_CHECKBOX_XPATH = '//*[@id="external-form"]/div[5]/div[1]'
LOGIN_BUTTON_XPATH = '//*[@id="external-form"]/button'
USER_LICENSE_CHECKBOX_ID = 'login__accept-eula'
USER_LICENSE_CHECKBOX_XPATH = '//*[@id="external-form"]/div[5]/div[1]'


def assertion_failed(context):
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    context.browser.execute_script('sauce:job-result=passed')


def find_password_field(context):
    password_field = context.browser.find_element_by_id(PASSWORD_FIELD_ID)
    return password_field


def find_username_field(context):
    username_field = context.browser.find_element_by_id(USERNAME_FIELD_ID)
    return username_field


def find_login_button(context):
    return context.browser.find_element_by_css_selector(LOGIN_BUTTON_SELECTOR)


def send_valid_username(usernameField):
    usernameField.send_keys(VALID_USERNAME)


def send_valid_password(passwordField):
    passwordField.send_keys(VALID_PASSWORD)


def find_copyright_checkbox(context):
    return context.browser.find_element(By.XPATH, COPYRIGHT_CHECKBOX_XPATH)


def login_with_valid_credentials(context):
    usernameField = find_username_field(context)
    usernameField.send_keys(VALID_USERNAME)
    passwordField = find_password_field(context)
    passwordField.send_keys(VALID_PASSWORD)
    copyright_checkbox = find_copyright_checkbox(context)
    copyright_checkbox.click()
    login_button = find_login_button(context)
    login_button.click()


def get_field_type(passwordField):
    return passwordField.get_attribute("type")


def check_for_error_message(context):
    time.sleep(1)
    assert context.browser.find_element_by_class_name(ERROR_MESSAGE_CLASS)
    context.browser.quit()


def find_show_password_button(context):
    return context.browser.find_element(By.XPATH, SHOW_PASSWORD_BUTTON)


def enter_valid_credentials(context):
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.ID, USERNAME_FIELD_ID)))
        username_field = find_username_field(context)
        send_valid_username(username_field)
        password_field = find_password_field(context)
        send_valid_password(password_field)
        context.browser.execute_script('sauce:job-result=passed')
    except TimeoutException:
        assertion_failed(context)


def verify_logged_into_platform(context):
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "menu-controls")))
        assert context.browser.find_element_by_class_name("menu-controls")
        assertion_passed(context)
    except TimeoutException:
        assertion_failed(context)


def nav_to_platform(context):
    context.browser.get(LOGIN_URL)


def find_license_checkbox(context):
    return context.browser.find_element_by_xpath(LICENSE_CHECKBOX_XPATH)


def get_login_button(context):
    return context.browser.find_element_by_xpath(LOGIN_BUTTON_XPATH)


def get_user_license_checkbox(context):
    return context.browser.find_element_by_xpath(USER_LICENSE_CHECKBOX_XPATH)
