Feature: Login to the platform

  Scenario: Login with valid credentials
    Given I am on the login page
    When I enter valid credentials
    When I click the license checkbox
    Then the checkbox is checked
    When I click log in
    Then I am logged in