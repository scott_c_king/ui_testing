#!/usr/bin/env python3

'''
Dragos Steps Module
'''

import time
from _support.PageObjects.LoginPage import *
from selenium import webdriver


use_step_matcher('re')

LOGIN_URL = "https://platform-dev10.dragos.services/#/login"
LICENSE_INPUT_CHECKBOX = '//*[@id="login__accept-eula"]'
USER_LICENSE_LINK_XPATH = '//*[@id="external-form"]/div[5]/div[2]/span'
LICENSE_POPUP_ID = 'modal__eula__content'


@given('I am on the login page')
def step_impl(context):
    nav_to_platform(context)


@when('I type the username "(.*)"')
def step_impl(context, username):
    usernameField = find_username_field(context)
    usernameField.send_keys(username)


@when('I type the password "(.*)"')
def step_impl(context, password):
    passwordField = find_password_field(context)
    passwordField.send_keys(password)


@when('I click log in')
def step_impl(context):
    login_button = find_login_button(context)
    login_button.click()


@then('I see an error message')
def step_impl(context):
    check_for_error_message(context)


@when('I enter valid credentials')
def step_impl(context):
    enter_valid_credentials(context)


@then('I am logged in')
def step_impl(context):
    verify_logged_into_platform(context)


@given('I am logged in')
def step_imp(context):
    nav_to_platform(context)
    login_with_valid_credentials(context)


@then('the password is masked')
def step_impl(context):
    password_field = find_password_field(context)
    field_type = get_field_type(password_field)
    try:
        assert field_type == "password"
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


@when('I click show password')
def step_impl(context):
    showPasswordButton = find_show_password_button(context)
    showPasswordButton.click()


@then('the password is not masked')
def step_impl(context):
    passwordField = find_password_field(context)
    fieldType = get_field_type(passwordField)
    try:
        assert fieldType == "text"
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


@given('I log back in')
def step_impl(context):
    nav_to_platform(context)
    login_with_valid_credentials(context)


@when('I click the license checkbox')
def step_impl(context):
    time.sleep(2)
    license_checkbox = find_license_checkbox(context)
    license_checkbox.click()
    time.sleep(2)


@when('I click on the user license link')
def step_impl(context):
    user_license_link = context.browser.find_element_by_xpath(USER_LICENSE_LINK_XPATH)
    user_license_link.click()


@then('the checkbox is checked')
def step_impl(context):
    license_checkbox = context.browser.find_element_by_xpath(LICENSE_INPUT_CHECKBOX).is_selected()
    try:
        assert license_checkbox
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)

@then('the checkbox is not checked')
def step_impl(context):
    time.sleep(1)
    license_checkbox = context.browser.find_element_by_xpath(LICENSE_INPUT_CHECKBOX).is_selected()
    try:
        assert license_checkbox == False
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)

@then('the user license appears')
def step_impl(context):
    time.sleep(1)
    license_popup = context.browser.find_element_by_id(LICENSE_POPUP_ID).is_displayed()
    try:
        assert license_popup
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)

@then('the login button is not clickable')
def step_impl(context):
    login_button = get_login_button(context).is_enabled()
    try:
        assert login_button == False
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)

@then('the login button is clickable')
def step_impl(context):
    login_button = get_login_button(context).is_enabled()
    try:
        assert login_button == True
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)

@then('I agree to the user license')
def step_impl(context):
    user_license_checkbox = get_user_license_checkbox(context)
    user_license_checkbox.click()
