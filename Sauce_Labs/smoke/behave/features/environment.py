#!/usr/bin/env python3

'''
Dragos Behave Environment
'''

import json
import os
import sys
from selenium import webdriver


URL = 'https://platform.dragos.services/#/login'
SL_HUB = ('https://ondemand.saucelabs.com:443/wd/hub')


try:
    BROWSERS = os.environ['CONFIG_FILE']
    TASK_ID = int(os.environ['TASK_ID'])
except NameError:
    print('This script should not be run stand-alone, but by '
          'behave_runner.py.  Browser configs (.json) and task ID '
          'should be passed as environment variables.\n\n'
          'Example: CONFIG_FILE=browsers.json TASK_ID=0 behave '
          'features/single.feature')
    sys.exit(1)


# Verify environment variables for Sauce Connect are present.
try:
    ENV = {}
    ENV['build'] = os.environ['SAUCE_BUILD_NAME']
    ENV['username'] = os.environ['SAUCE_USERNAME']
    ENV['accessKey'] = os.environ['SAUCE_ACCESS_KEY']
    ENV['tunnelIdentifier'] = os.environ['TUNNEL_IDENTIFIER']
except KeyError:
    print('ERROR: Missing required Jenkins environment variables!')
    sys.exit(1)


with open(BROWSERS, 'r') as data_file:
    CONFIG = json.load(data_file)


def before_feature(context, feature):
    ''' Instantiate webdriver before each feature. '''
    capabilities = CONFIG['browsers'][TASK_ID]

    # Add the local/Jenkins environment variables
    capabilities.update(ENV)
    print('INFO: Desired Capabilities: ', capabilities)

    context.browser = webdriver.Remote(command_executor=SL_HUB,
                                       desired_capabilities=capabilities)

    # Required for Sauce Labs/Jenkins reporting
    session_id = context.browser.session_id
    print(f'SauceOnDemandSessionID={session_id} job-name={ENV["build"]}')


def after_feature(context, feature):
    ''' Quit webdriver instance after the feature. '''
    context.browser.quit()
