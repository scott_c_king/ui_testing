#!/usr/bin/env python3

'''
BrowserStack Behave Runner
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

import json
import sys
import subprocess


def number_of_browsers(browser_configs):
    '''
    Import JSON, return number of browser configs.
    '''
    with open(browser_configs, 'r') as jfile:
        data = json.loads(jfile.read())
    return len(data['browsers'])


def import_json(browser_configs):
    '''
    Import JSON, return Python dictionary.
    '''
    with open(browser_configs, 'r') as jfile:
        data = json.loads(jfile.read())
    return data


def main():
    ''' Do the work. '''

    browsers = number_of_browsers(BROWSER_CONFIGS)
    processes = []
    for counter in range(browsers):
        cmd = (f'CONFIG_FILE={BROWSER_CONFIGS} '
               f'TASK_ID={counter} behave '
               f'{FEATURE_FILE}')
        print(cmd)
        processes.append(subprocess.Popen(cmd, shell=True))

    for counter in range(browsers):
        processes[counter].wait()


if __name__ == "__main__":
    try:
        BROWSER_CONFIGS = sys.argv[1]
        FEATURE_FILE = sys.argv[2]
    except IndexError:
        print('Browser configs (.json) and feature file should be passed '
              'as the first and second arguments, respectively. Use absolute '
              'paths when/if necessary.\n\n'
              'Example: python3 behave_runner.py behave_browsers.json '
              'features/single.feature')
        sys.exit(1)
    main()
