#!/usr/bin/env python3

'''
Sauce Labs Local Testing POC
'''

import os
import sys
import urllib3
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
urllib3.disable_warnings()

try:
    SAUCE_BUILD_NAME = os.environ['SAUCE_BUILD_NAME']
    USERNAME = os.environ['SAUCE_USERNAME']
    SAUCE_ACCESS_KEY = os.environ['SAUCE_ACCESS_KEY']
    SAUCE_TUNNEL_ID = os.environ['TUNNEL_IDENTIFIER']
except KeyError:
    print('ERROR: Missing required Jenkins environment variables!')
    sys.exit(1)

# Sauce Labs Selenium Hub
REMOTE_URL = 'https://ondemand.saucelabs.com:443/wd/hub'
URL = 'https://platform.dragos.services/#/login'
CAPABILITIES = {'platform': 'macOS 10.15',
                'browserName': 'firefox',
                'version': '73.0',
                'build': SAUCE_BUILD_NAME,
                'username': USERNAME,
                'accessKey': SAUCE_ACCESS_KEY,
                'tunnelIdentifier': SAUCE_TUNNEL_ID}


def main():
    ''' Sauce Labs Local Testing POC '''

    try:
        driver = webdriver.Remote(command_executor=REMOTE_URL,
                                  desired_capabilities=CAPABILITIES)

        # Required for Sauce Labs/Jenkins reporting
        session_id = driver.session_id
        print(f'SauceOnDemandSessionID={session_id} '
              f'job-name={SAUCE_BUILD_NAME}')

        driver.get(f'{URL}')
        print('TEST CASE: Is Dragos found in page title?')
        assert 'Dragos' in driver.title
        print(f'PASS: Dragos is found in "{driver.title}" for URL {URL}?')
        driver.execute_script('sauce:job-result=passed')
    except WebDriverException:
        print(f'FAIL: Dragos not found in "{driver.title}" for URL {URL}.')
        driver.execute_script('sauce:job-result=failed')
    finally:
        driver.quit()
        print('INFO: driver.quit() completed.')


if __name__ == "__main__":
    main()
