#!/usr/bin/env python3

'''
Sauce Labs Parallel Instance Runner
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

import json
import sys
import subprocess
from subprocess import check_output


def number_of_browsers(browser_configs):
    '''
    Import JSON, return number of browser configs.
    '''
    with open(browser_configs, 'r') as jfile:
        data = json.loads(jfile.read())
    return len(data)


def import_json(browser_configs):
    '''
    Import JSON, return Python dictionary.
    '''
    with open(browser_configs, 'r') as jfile:
        data = json.loads(jfile.read())
    return data


def main():
    ''' Sauce Labs Parallel Instance Runner '''

    browsers = number_of_browsers(BROWSER_CONFIGS)
    browser_capabilities = import_json(BROWSER_CONFIGS)
    processes = []
    for counter in range(browsers):
        cmd = 'python3 %s %s %s' % (TEST_SCRIPT, BROWSER_CONFIGS, counter)
        info = '\nINFO: Now starting instance [{}] with capabilities:\n{}'
        print(info.format(counter, browser_capabilities[counter]))
        processes.append(subprocess.Popen(cmd, shell=True))

    for counter in range(browsers):
        processes[counter].wait()


if __name__ == "__main__":
    try:
        TEST_SCRIPT = sys.argv[1]
        BROWSER_CONFIGS = sys.argv[2]
    except IndexError:
        print('Test script and browser configs (.json) should be passed '
              'as the first and second arguments, respectively. Use absolute '
              'paths when/if necessary.\n\n'
              'Example: python3 sl_prunner.py mytestscript.py browsers.json')
        sys.exit(1)
    main()
