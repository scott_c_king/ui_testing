#!/usr/bin/env python3

'''
Sauce Labs Local (4) Instance Parallel Test POC
'''

import json
import os
import sys
import urllib3
from selenium import webdriver
urllib3.disable_warnings()


URL = 'https://platform.dragos.services/#/login'
SL_HUB = ('https://ondemand.saucelabs.com:443/wd/hub')


def import_json(browser_configs, process_index):
    '''
    Import JSON, return Python dictionary by process_index.
    '''
    with open(browser_configs, 'r') as jfile:
        data = json.loads(jfile.read())
    return data[process_index]


def main():
    ''' Sauce Labs Local (4) Instance Parallel Test POC '''

    # Read browers.json, load correct capabilities index.
    capabilities = import_json(BROWSERS, PROCESS_INDEX)

    # Add the local environment variables
    capabilities.update(ENV)

    # Start Webdriver
    driver = webdriver.Remote(command_executor=SL_HUB,
                              desired_capabilities=capabilities)

    # Required for Sauce Labs/Jenkins reporting
    session_id = driver.session_id
    print(f'SauceOnDemandSessionID={session_id} job-name={ENV["build"]}')

    # Test
    driver.get(f'{URL}')
    print(f'\nProcess [{PROCESS_INDEX}]: Is Dragos found in page title?')
    result = 'Process [{}]: {} - Dragos {} found in "{}" for URL {}.'
    try:
        assert 'Dragos' in driver.title
        print(result.format(PROCESS_INDEX, 'PASS', 'is', driver.title, URL))
        driver.execute_script('sauce:job-result=passed')
    except AssertionError:
        print(result.format(PROCESS_INDEX, 'FAIL', 'not', driver.title, URL))
        driver.execute_script('sauce:job-result=failed')
    finally:
        driver.quit()
        print(f'Process [{PROCESS_INDEX}]: Webdriver stopped...')


if __name__ == "__main__":

    # Verify arguments passed from PRunner are present.
    try:
        BROWSERS = sys.argv[1]
        PROCESS_INDEX = int(sys.argv[2])
    except IndexError:
        print('This script should not be run stand alone, but by sl_prunner.'
              'Browser configs (.json) and process index should be passed'
              'as the first and second arguments, respectively.\n\n'
              'Example: python3 sl_ptest.py browsers.json process_index')
        sys.exit(1)

    # Verify environment variables for Sauce Connect are present.
    try:
        ENV = {}
        ENV['build'] = os.environ['SAUCE_BUILD_NAME']
        ENV['username'] = os.environ['SAUCE_USERNAME']
        ENV['accessKey'] = os.environ['SAUCE_ACCESS_KEY']
        ENV['tunnelIdentifier'] = os.environ['TUNNEL_IDENTIFIER']
    except KeyError:
        print('ERROR: Missing required Jenkins environment variables!')
        sys.exit(1)

    main()
