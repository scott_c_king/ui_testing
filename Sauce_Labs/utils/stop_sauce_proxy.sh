#!/bin/bash

# Stop Sauce Labs Connect Proxy
kill `ps -ef | grep dragos_qa | grep sc-proxy-tunnel | awk 'NR==1 { print$2 }'`
