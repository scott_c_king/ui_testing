#!/usr/bin/env python3

'''
Sauce Labs Test: Run (4) Instances Serially
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

import sys
from subprocess import check_output

CMDS = [
    'python3 sl_local_single_test_1.py | tee logs/sl_local_single_test_1.log',
    'python3 sl_local_single_test_2.py | tee logs/sl_local_single_test_2.log',
    'python3 sl_local_single_test_3.py | tee logs/sl_local_single_test_3.log',
    'python3 sl_local_single_test_4.py | tee logs/sl_local_single_test_4.log'
    ]


def sauce_proxy_check():
    '''
    Verify Sauce Labs Proxy is running.
    '''
    cmd = "ps -ef | grep dragos_qa | grep sc-proxy-tunnel | awk '{ print$2 }'"
    process = check_output(cmd, shell=True).decode('utf-8').split()
    if len(process) <= 1:
        raise IndexError
    return True


def main():
    ''' Sauce Labs Test: Run (4) Instances Serially '''
    try:
        sauce_proxy_check()
        print('INFO: Sauce Labs Proxy is running.')
    except IndexError:
        print('ERROR: Sauce Labs Proxy is not running.\n\n'
              'Run this and re-try:\n'
              'cd ../utils/ ; ./start_sauce_proxy.sh ; cd ../serial')
        sys.exit(1)

    for cmd in CMDS:
        check_output(cmd, shell=True)


if __name__ == "__main__":
    main()
