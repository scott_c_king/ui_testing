#!/usr/bin/env python3

'''
Sauce Labs Local Testing POC
'''

import sys
from subprocess import check_output
import urllib3
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
urllib3.disable_warnings()

# Sauce Labs Selenium Hub
REMOTE_URL = "https://ondemand.saucelabs.com:443/wd/hub"
URL = 'https://platform.dragos.services/#/login'
CAPABILITIES = {'browserName': 'chrome',
                'platform': 'Windows 7',
                'version': '78.0',
                'screenResolution': '1920x1200',
                'build': 'SL Local: Serial Test',
                'name': 'SL Local: W7Chr78',
                'username': 'dragos_qa',
                'accessKey': '674552f5-1157-427c-ae6f-4ffd07cf523a',
                # tunnelIdentifier - required for Sauce Connect Proxy tunnel
                'tunnelIdentifier': 'sc-proxy-tunnel'}
# Can't get this to work ... investigating.
# 'sauce:options': {'screenResolution': '1920x1080'}


def sauce_proxy_check():
    '''
    Verify Sauce Labs Proxy is running.
    '''
    cmd = "ps -ef | grep dragos_qa | grep sc-proxy-tunnel | awk '{ print$2 }'"
    process = check_output(cmd, shell=True).decode('utf-8').split()
    if len(process) <= 1:
        raise IndexError
    return True


def main():
    ''' Sauce Labs Local Testig POC '''

    try:
        sauce_proxy_check()
        print('INFO: Sauce Labs Proxy is running.')
    except IndexError:
        print('ERROR: Sauce Labs Proxy is not running.\n\n'
              'Run this and re-try:\n'
              'cd ../utils/ ; ./start_sauce_proxy.sh ; cd ../single')
        sys.exit(1)

    try:
        driver = webdriver.Remote(command_executor=REMOTE_URL,
                                  desired_capabilities=CAPABILITIES)
        driver.get(f'{URL}')
        print('TEST CASE: Is Dragos found in page title?')
        assert 'Dragos' in driver.title
        print(f'PASS: Dragos is found in "{driver.title}" for URL {URL}?')
        driver.execute_script('sauce:job-result=passed')
    except WebDriverException:
        print(f'FAIL: Dragos not found in "{driver.title}" for URL {URL}.')
        driver.execute_script('sauce:job-result=failed')
    finally:
        driver.quit()
        print('INFO: driver.quit() completed.')


if __name__ == "__main__":
    main()
