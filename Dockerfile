FROM python:3.8.2-buster

# Update container
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git && \
    pip3 install --upgrade pip

# Install UI testing requirements
COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

# Drop into bash
CMD ["/bin/bash"]
