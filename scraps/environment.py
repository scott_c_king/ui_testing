from selenium import webdriver
from browserstack.local import Local

import json
import os
import sys

# python3 behave_runner.py browsers.json features/single.feature
# CONFIG_FILE=browsers.json TASK_ID=8 behave features/single.feature

#CONFIG_FILE = os.environ['CONFIG_FILE'] if 'CONFIG_FILE' in os.environ else 'config/single.json'
#TASK_ID = int(os.environ['TASK_ID']) if 'TASK_ID' in os.environ else 0

print('INFO: OS Environ:',os.environ)

try:
    browsers = os.environ['CONFIG_FILE']
    task_id = int(os.environ['TASK_ID'])
except NameError:
    print('This script should not be run stand-alone, but by '
          'behave_runner.py.  Browser configs (.json) and task ID '
          'should be passed as environment variables.\n\n'
          'Example: CONFIG_FILE=browsers.json TASK_ID=0 behave '
          'features/single.feature')
    sys.exit(1)

print('INFO: Config File:', browsers)
print('INFO: Task ID: ', task_id)

with open(browsers) as data_file:
    CONFIG = json.load(data_file)

#bs_local = None

BROWSERSTACK_USERNAME = os.environ['BROWSERSTACK_USERNAME'] if 'BROWSERSTACK_USERNAME' in os.environ else CONFIG['user']
print('BrowserStack Username:', BROWSERSTACK_USERNAME)
BROWSERSTACK_ACCESS_KEY = os.environ['BROWSERSTACK_ACCESS_KEY'] if 'BROWSERSTACK_ACCESS_KEY' in os.environ else CONFIG['key']
print('BrowserStack Access Key:', BROWSERSTACK_ACCESS_KEY)

#def start_local():
#    """Code to start browserstack local before start of test."""
#    global bs_local
#    bs_local = Local()
#    bs_local_args = { "key": BROWSERSTACK_ACCESS_KEY, "forcelocal": "true" }
#    bs_local.start(**bs_local_args)
#
#def stop_local():
#    """Code to stop browserstack local after end of test."""
#    global bs_local
#    if bs_local is not None:
#        bs_local.stop()


def before_feature(context, feature):
    ''' Blah '''
    desired_capabilities = CONFIG['browsers'][task_id]
    print('INFO: Desired Capabilities: ', desired_capabilities)

    for key in CONFIG["capabilities"]:
        print('INFO: Key:', key)
        if key not in desired_capabilities:
            desired_capabilities[key] = CONFIG["capabilities"][key]

    # We're manually starting browserstack.local before this runs
    #if "browserstack.local" in desired_capabilities and desired_capabilities["browserstack.local"]:
    #    start_local()

    BS_HUB = ('https://scottcking1:9A7XsA7QzPmspiSwrsUc@'
              'hub-cloud.browserstack.com/wd/hub')
    context.browser = webdriver.Remote(desired_capabilities=desired_capabilities,
                                       command_executor=BS_HUB)

def after_feature(context, feature):
    context.browser.quit()
    #stop_local()
